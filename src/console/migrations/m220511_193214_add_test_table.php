<?php

use yii\db\Migration;

/**
 * Class m220511_193214_add_test_table
 */
class m220511_193214_add_test_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('test', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);



        $teste = new \common\models\Test();
        $teste->name = 'teste1';
        $teste->save();


        $teste = new \common\models\Test();
        $teste->name = 'teste2';
        $teste->save();


        return true;

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('test');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220511_193214_add_test_table cannot be reverted.\n";

        return false;
    }
    */
}
