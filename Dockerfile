FROM felipezcanal/web-php71
ARG BUILD_ENV=Development
ADD src /app
RUN cd /app && php init --env=${BUILD_ENV} --overwrite=All