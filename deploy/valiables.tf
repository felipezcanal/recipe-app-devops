variable "prefix" {
  default = "rad"
}

variable "project" {
  default = "recipe-app-devops"
}

variable "contact" {
  default = "felipe.z.canal@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for the API"
  default     = "173133661961.dkr.ecr.us-east-1.amazonaws.com/recipe-app-devops:latest"
}

variable "ecr_image_console" {
  description = "ECR image for the console"
  default     = "173133661961.dkr.ecr.us-east-1.amazonaws.com/recipe-console-devops:latest"
}

variable "django_secret_key" {
  description = "secret keu for django app"
}

variable "project_webroot" {
  description = "webroot for the project"
  default     = "/app/frontend/web"
}